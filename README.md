# Coulombic

Coulombic is a learning game made for the MIT edX online course "Design and Development of Games for Learning". It's intended to introduce high school students to electrostatics. Unlike most education games, this game is free and open source which means you can copy it, modify it, or learn how it was made.

![screenshot 1](doc/images/screenshot1.png)

# Quickstart Guide

### Blender

You may need to install the free and open source software (FOSS) called [Blender](https://www.blender.org/download/), unless you already have some other 3D editor installed. This allows your computer to understand the `.blend` files in the project (3D models, 3D environment).

### Unity3D

Next, install the game making tool called [Unity3D](https://unity3d.com/get-unity/download/archive). Unity3D is also free of charge, however it is not FOSS. It may be a good idea to install the same (or similar) version of Unity3D as the one used by the Coulombic project. You can see which version of Unity3D Coulombic uses by examining this file: `ProjectSettings/ProjectVersion.txt`.

### Open the project

After downloading the Coulombic project files from this web page, start Unity3D and use it to open these project files.

You may need to navigate to the "Scenes" folder, double click the "Main Menu" scene to open it, then at the top of the screen, press the play button.

You can also make releases for Windows, MacOS, Linux, web, and other platforms. That way, others don't need to install Blender and Unity to play the game. To do that, you can look up generic Unity3D tutorials and guides.

# Media

* See [here](https://www.youtube.com/watch?v=EzUWMpP99DY) for a short gameplay tour.
* See [here](https://www.youtube.com/watch?v=eBVim8kEhK8) for an overview of the Unity3D, C# code, and programming concepts I used to build the game.

# Contact

Thanks for your interest! I'm always very happy to hear when teachers use this project to help teach in a classroom. Feel free to send me an email if you have thoughts or feedback:

![contact info](doc/images/email.png)
